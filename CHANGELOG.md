# Greenish Color Theme - Change Log

## [0.3.5]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.3.4]

- update readme and screenshot

## [0.3.3]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode
- focusborder to match theme
- define window border in custom mode
- fix menu FG/BG

## [0.3.2]

- fix manifest and pub WF

## [0.3.1]

- match statusbar FG/BG

## [0.3.0]

- dimmer button BG so as to not compete visually with the project names in the Git sidebar
- fix manifest repo links
- retain v0.2.4 for those who prefer the earlier style

## [0.2.4]

- make lst act sel BG transparent

## [0.2.3]

- fix syntax FG contrast on exceptional cases:
illegal
Broken
Deprecated
Unimplemented

## [0.2.2]

- fix scrollbar, minimap slider transparencies:
light:
"minimapSlider.activeBackground": "#00000040",
"minimapSlider.background": "#00000020",
"minimapSlider.hoverBackground": "#00000060",
"scrollbarSlider.activeBackground": "#00000040",
"scrollbarSlider.background": "#00000020",
"scrollbarSlider.hoverBackground": "#00000060",
- fix completion list contrast:
editorSuggestWidget.selectedBackground darkened
list.hoverBackground OK

## [0.2.1]

- fix input border

## [0.2.0]

- retain v0.1.3 for those who prefer the previous style
- use more consistent BG colors throughout
- fix shadows
- fix borders
- fix badges
- reverse FG/BG
- fix tab highlights

## [0.1.3]

- fix contrast of notification, hover widget FG/BG

## [0.1.2]

- hover widget FG/BG

## [0.1.1]

- notification FG/BG

## [0.1.0]

- tab borders
- scrollbar and widget shadows
- lighten background of editor and general panels, for contrast
- fix color match in activity bar, list, commands, find
- use syntax colors from humane-like
- keep old version of .VSIX for those who like original colors

## [0.0.3]

- scale icon down
- chg icon colorspace

## [0.0.2]

- rename pub and add manifest values

## [0.0.1]

- Initial release
