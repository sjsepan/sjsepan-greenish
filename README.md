# Greenish Theme

Greenish light color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-greenish_code.png](./images/sjsepan-greenish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-greenish_codium.png](./images/sjsepan-greenish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-greenish_codedev.png](./images/sjsepan-greenish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-greenish_ads.png](./images/sjsepan-greenish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-greenish_theia.png](./images/sjsepan-greenish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-greenish_positron.png](./images/sjsepan-greenish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/11/2025
